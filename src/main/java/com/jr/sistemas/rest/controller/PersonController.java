package com.jr.sistemas.rest.controller;



import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jr.sistemas.rest.domain.Person;
import com.jr.sistemas.rest.domain.PersonList;
import com.jr.sistemas.rest.service.PersonService;

@Controller
public class PersonController 
{
	protected static Logger logger=Logger.getLogger("controller");
	
	//Para que este objeto no sea nulo , le ponemos la siguiente anotacion , previamente definida en PersonService
	@Resource(name="personService")
	private PersonService personService;
	
	// Para que atienda solicitudes request usuamos @RequestMapping , con el atributo headers le indico que puede soportar
	// La anotacion @ResponseBody hace que lo que se muestre ya no sea una vista , sino xml o json dependiendo.
	
	
	@RequestMapping(value="/persons" , method=RequestMethod.GET,headers="Accept=application/xml, application/json" )
	private @ResponseBody PersonList getPersons()
	{
		PersonList result=new PersonList();
		result.setData(personService.getAll());
		return result;
	}
	
	/*
	 * La anotacion @PathVariable , permite recibir el "id" que se mapean en el @RequestMapping(value="/person/{id}")  
	 * El @RespondBody convierte la salida de la vista que exige el spring mvc a salida xml o json
	 */
	@RequestMapping(value="/person/{id}" , method=RequestMethod.GET,headers="Accept=application/xml, application/json" )
	private @ResponseBody Person getPersona(@PathVariable("id") Long id)
	{
		logger.debug("El proveedor ha recibido una solicitud para obtener una persona con id: "+id);
		return personService.getPerson(id);
	}
	
	/*
	 * M�todo que obtiene una foto
	 */
	@RequestMapping(value="/person/{id}", method=RequestMethod.GET, headers="Accept=image/jpeg,image/jpg,image/png,image/gif")
	public @ResponseBody byte[] getPhoto(@PathVariable("id")  Long id)
	{
		// Aqu� llama al servicio
		try {
			//Recupera imangen del Classpath
			InputStream is=this.getClass().getResourceAsStream("/bella.jpg");
			// Imagen del buffer preparada
			BufferedImage img=ImageIO.read(is);
			//Creando un buffer de salida de array de byte
			ByteArrayOutputStream bao=new ByteArrayOutputStream();
			// Escribir en el flujo de salida
			ImageIO.write(img, "jpg", bao);
			logger.debug("Recuperando foto como imagen de array de byte");
			return bao.toByteArray();
		} catch (IOException e) {
			logger.debug(e);
			throw new RuntimeException();
		}
	}
	
	/*
	 *M�todo que devuelve una vista  
	 */
	@RequestMapping(value="/person/{id}" , method=RequestMethod.GET,headers="Accept=application/html, application/xhtml+xml" )
	public String getPersonHTML(@PathVariable("id") Long id , Model model)
	{
		logger.debug("El proveedor ha recibido una solicitud para obtener una persona con id: "+id);
		//Aqu� inicia el servicio
		model.addAttribute("person",personService.getPerson(id));
		return "getPage";
	}
	
	/*
	 * M�todo para agregar una persona
	 */
	@RequestMapping(value="/person" , method=RequestMethod.POST,headers="Accept=application/xml, application/json" )
	public @ResponseBody Person addPerson(@RequestBody Person person)
	{
		logger.debug("El proveedor ha recibido una solicitud para agregar una nueva persona ");
		//Aqu� inicia el servicio
		return personService.add(person);
	}
	
	/*
	 * M�todo para actualizar una persona
	 */
	
	@RequestMapping(value="/person/{id}" , method=RequestMethod.PUT ,headers="Accept=application/xml, application/json" )
	public @ResponseBody String updatePerson(@PathVariable("id") Long id, @RequestBody Person person)
	{
		logger.debug("El proveedor ha recibido una solicitud para editar una persona con id:  "+id);
		//Aqu� inicia el servicio
		person.setId(id);
		return personService.edit(person).toString();
	}
	/*
	 * M�todo para eliminar una persona
	 */
	@RequestMapping(value="/person/{id}" , method=RequestMethod.DELETE ,headers="Accept=application/html, application/xhtml+xml" )
	public @ResponseBody String deletePerson(@PathVariable("id") Long id)
	{
		logger.debug("El proveedor ha recibido una solicitud para borrar una persona con id:  "+id);
		//Aqu� inicia el servicio
		
		return personService.delete(id).toString();
	}
	
	
}
