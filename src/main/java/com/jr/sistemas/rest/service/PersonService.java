package com.jr.sistemas.rest.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.jr.sistemas.rest.domain.Person;


@Service("personService")
public class PersonService 
{
	protected static Logger logger=Logger.getLogger("service");
	private List<Person> persons=new ArrayList<Person>();
	
	public PersonService() 
	{
		logger.debug("Iniciando DB");
		Person person1=new Person();
		person1.setId(0L);
		person1.setFirstName("Lucas");
		person1.setLastName("Mamani");
		person1.setMoney(1500.50);
		
		Person person2=new Person();
		person2.setId(1L);
		person2.setFirstName("Checha");
		person2.setLastName("Mamani");
		person2.setMoney(1700.80);
		
		Person person3=new Person();
		person3.setId(2L);
		person3.setFirstName("Panfila");
		person3.setLastName("Mamani");
		person3.setMoney(3800.50);
		
		Person person4=new Person();
		person4.setId(4L);
		person4.setFirstName("Silvestre");
		person4.setLastName("Mamani");
		person4.setMoney(5400.50);
		
		persons.add(person1);
		persons.add(person2);
		persons.add(person3);
		persons.add(person4);
	}
	/* 
	 * M�todo para listar personas
	 */
	public List<Person> getAll()
	{
		logger.debug("Obteniendo lista de personas");
		return persons;
	}
	/*
	 * M�todo para encontrar una persona
	 */
	
	public Person getPerson(Long id)
	{
		logger.debug("Recuperar persona con id :"+id);
		for (Person person : persons) 
		{
			if (person.getId().longValue()==id.longValue()) {
				logger.debug("valor encontrado");
				return person;
			}
		}
		logger.debug("No hay registros encontrados");
		return null;
	}
	/*
	 * M�todo para agregar una persona
	 */
	public Person add(Person person)
	{
		logger.debug("Agregando persona");
		try {
			Long nuevoID=0L;
			Boolean valorEncontradoCorrecto=false;
			while(valorEncontradoCorrecto=false)
			{
				for(int i=0;i<persons.size();i++)
				{
					if (persons.get(i).getId().longValue()==nuevoID.longValue()) {
						nuevoID++;
						break;
					}
					
					if (i==persons.size()-1) {
						logger.debug("Asignando id: "+nuevoID);
						valorEncontradoCorrecto=true;
					}
				}
			}
			//Asignando nuevo id
			person.setId(nuevoID);
			//Agregando a la lista
			persons.add(person);
			logger.debug("Se agrego nueva persona");
			return person;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
		
	}	
	/*
	 * M�todo para borrar una persona
	 */
	
	public Boolean delete(Long id)
	{
		logger.debug("Borrando person con id : "+id);
		try {
			for (Person person : persons) {
				if (person.getId().longValue()==id.longValue()) {
					logger.debug("Registro encontrado");
					persons.remove(person);
					return true;
				}
			}
			
			logger.debug("Registro no encontrado");
			return false;
			
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
	
	/*
	 * M�todo para editar una persona
	 */
	
	public Boolean edit(Person person)
	{
		logger.debug("ditando persona con id: "+person.getId());
		try {
			for (Person p: persons) {
				if (p.getId().longValue()==person.getId().longValue()) {
					logger.debug("Registro encontrado");
					persons.remove(p);
					persons.add(person);
					return true;
				}
			}
			logger.debug("Registro no encontrado");
			return false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
		
	}
	
}
